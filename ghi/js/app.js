function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${start}-${end}</div>
      </div>
    `;
  }

  function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
  }




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    let x = 0;
    const columns = document.querySelectorAll('.col');
    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = new Date(details.conference.starts)
                const ends = new Date(details.conference.ends)
                const location = details.conference.location.name
                const start = starts.toLocaleDateString()
                const end = ends.toLocaleDateString()
                const html = createCard(title, description, pictureUrl, start, end, location);
                const column = columns[x % 3]
                // const column = document.querySelector('.col');
                x = (x + 1) % 3
                column.innerHTML += html;
                console.log(starts.toDateString())
            }
        }

      }

    } catch (e) {
        console.error(e);
        //Figure out what do if an error is raised

        if (alertTrigger) {
            alertTrigger.addEventListener('click', function () {
              alert('Nice, you triggered this alert message!', 'success')
            })
          }

    }
});
