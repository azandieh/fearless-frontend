import Nav from './Nav';
import React from "react";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
          <Route path="conferences">
            <Route path="/conferences" element={<ConferenceForm />} />
          <Route index element={<MainPage />} />
          </Route>
          <Route path="attendees" >
            <Route path="/attendees" element={<AttendConferenceForm />} />
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
        </Routes>
        {/*<AttendeesList attendees = {props.attendees} /> */}
    </BrowserRouter>
  );
}

export default App;
