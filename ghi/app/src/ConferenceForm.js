import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  //Notice that we can condense all formData
  //into one state object
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [max_presentations, setMaximumPresentations] = useState('');
  const [max_attendees, setMaximumAttendees] = useState('');
  const [location, setLocation] = useState('');


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.location = location;
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = max_presentations;
    data.max_attendees = max_attendees;

    const url = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data

        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaximumPresentations('');
        setMaximumAttendees('');
        setLocation('');



    }
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleChangeStarts = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const handleChangeEnds = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const handleChangeDescription = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleChangeMaxPresentations = (event) => {
    const value = event.target.value;
    setMaximumPresentations(value);
  }

  const handleChangeMaxAttendees = (event) => {
    const value = event.target.value;
    setMaximumAttendees(value);
  }

  const handleChangeLocation = (event) => {
    const value = event.target.value;
    setLocations(value);
  }




  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeStarts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeEnds} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleChangeDescription} className="form-control" id="description" rows="3" name="description" ></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeMaxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeMaxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
    </div>
      </div>

  )
}
export default ConferenceForm;
